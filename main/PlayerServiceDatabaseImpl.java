package main;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class PlayerServiceDatabaseImpl implements PlayerService {
	String user,password,driver,url;
	Connection connection;
	public PlayerServiceDatabaseImpl(String user,String password,String driver,String url) {
		this.user=user;
		this.password=password;
		this.driver=driver;
		this.url=url;
	}
	private Connection getConnection() throws ClassNotFoundException,SQLException{
		Class.forName(driver);
		return DriverManager.getConnection(url,user,password);
	}
	@Override
	public void add(Player player) {
		// TODO Auto-generated method stub
		try {
		connection = getConnection();
		//String query = "INSERT INTO player(Player_Name,Team_Name,Player_Number,Date_Of_Birth,Gender) VALUES(?,?,?,?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO player(Player_Name,Team_Name,Player_Number,Date_Of_Birth,Gender) VALUES(?,?,?,?,?)");
		preparedStatement.setString(1,player.getPlayername());
		preparedStatement.setString(2,player.getTeamname());
		preparedStatement.setInt(3,player.getPlayernumber());
		preparedStatement.setDate(4,Date.valueOf(player.getBirthDate()));
		preparedStatement.setString(5,player.getGender().toString());
		preparedStatement.executeUpdate();
		connection.close(); } catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		} 

		


	}

	@Override
	public void displayElements() {
		// TODO Auto-generated method stub
		try {
			Connection connection=getConnection();
			connection = getConnection();
			//String query = "SELECT * FROM player;";
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM player;");
			ResultSet resultSet=preparedStatement.executeQuery();
			while(resultSet.next())
			{
				Player player=getPlayer(resultSet);
				System.out.println(player) ;
			}
			connection.close();

			} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			}

			
	}

	private Player getPlayer(ResultSet resultSet) throws SQLException {
		Player player = new Player();
		player.setPlayername(resultSet.getString("player_name"));
		player.setTeamname(resultSet.getString("team_name"));
		player.setPlayernumber(resultSet.getInt("player_number"));
		player.setBirthdate(resultSet.getDate("date_of_birth").toLocalDate());
		player.setGender(Gender.valueOf(resultSet.getString("gender")));
		return player;
	}
	@Override
	public void searchElement(String name) {
		// TODO Auto-generated method stub
		try {
			Connection connection=getConnection();
			connection = getConnection();
			//String query = "SELECT * FROM player;";
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM player where player_name=?;");
			preparedStatement.setString(1,name);
			ResultSet resultSet=preparedStatement.executeQuery();
			while(resultSet.next())
			{
				Player player=getPlayer(resultSet);
				System.out.println(player) ;
			}
			connection.close();

			} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			}
	}

	@Override
	public void sorting() {
		// TODO Auto-generated method stub
		Connection connection;
		try {
		connection = getConnection();
		PreparedStatement prestatement = connection.prepareStatement("select * from player order by player_number asc");
		ResultSet resultSet = prestatement.executeQuery();
		while (resultSet.next()) {
			Player player = getPlayer(resultSet);
		System.out.println(player);



		}
		resultSet.close();
		connection.close();



		} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
	}

	@Override
	public List<Player> searchDate(LocalDate date) {
		// TODO Auto-generated method stub
		List<Player> playerList=new ArrayList<>();
		try {
			Connection connection=getConnection();
			connection = getConnection();
			//String query = "SELECT * FROM player;";
			PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM player where date_of_birth=?;");
			preparedStatement.setDate(1,Date.valueOf(date));
			ResultSet resultSet=preparedStatement.executeQuery();
			while(resultSet.next())
			{
				playerList.add(getPlayer(resultSet));
			}
			connection.close();

			} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			}
		return playerList;
	}
	

	@Override
	public void playerDownload() throws Exception {
		// TODO Auto-generated method stub
		Connection connection;
		try {
			connection = getConnection();
			PreparedStatement prestatement = connection.prepareStatement("select * from player");
			ResultSet resultSet=prestatement.executeQuery();
			FileWriter file=new FileWriter("playerDetails.csv");
			while(resultSet.next()) {
				String data = csvConversion(getPlayer(resultSet));
				file.write(data);
			}
			file.close();
			} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
	}

	@Override
	public void playerDownloadJson() throws Exception {
		// TODO Auto-generated method stub
		Connection connection;
		try {
			connection = getConnection();
			PreparedStatement prestatement = connection.prepareStatement("select * from player");
			ResultSet resultSet=prestatement.executeQuery();
			FileWriter file=new FileWriter("playerDetails.Json");
			StringJoiner join = new StringJoiner(",", "[", "]");
			while(resultSet.next()) {
				String data = asJson(getPlayer(resultSet));
				join.add(data);
			}
			file.write(join.toString());
			file.close();
			} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
	}


	@Override
	public void delete(String name) {
		// TODO Auto-generated method stub
		Connection connection;
		try {
			connection = getConnection();
			PreparedStatement prestatement = connection.prepareStatement("DELETE from player where player_name=?");
			prestatement.setString(1, name);
			prestatement.executeUpdate();
			System.out.println("Successfully Deleted");



			connection.close();



			} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
}
}
