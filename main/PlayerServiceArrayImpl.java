package main;
import java.io.FileWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
public  class PlayerServiceArrayImpl implements PlayerService {
	private Player []players;
	private int j=0;
	public PlayerServiceArrayImpl() {
		players=new Player[100];
	}
	public PlayerServiceArrayImpl(int size) {
		players=new Player[size];
	}
	/*The Method will Add the Player Details
	 */
	public void add(Player player) {
		boolean flag=true;
		if(j==players.length)
		{
			System.out.println("Limit Exceeded");
		}
		else //if(flag==true)
		{
		for(int i=0;i<j;i++) {
			if(players[i].equals(player)) {
			System.out.println("Duplicate Data");
			flag=false;
			}
		}
		}
		if(flag==true)
		{
			players[j]=player;
			j++;
		}
	}
	/* The Method will Display the Player Details*/
	public void displayElements() {
		if(j==0)
			System.out.println("no data");
		else
		{
			for(int i=0;i<j;i++)
			{
				System.out.println(players[i]);
			}
		}
	}
	/*The Method will Search for Player
	 * @param name
	 */
	public void searchElement(String name) {
		boolean flag=false;
			for(int i=0;i<j;i++)
			{
				if(players[i].getPlayername().equals(name)) {
					System.out.println(players[i]);
					flag=true;	
				}
			}
		if(flag==false)
		{
		System.out.println("no data found");
		}
		
	}
	/*The Method will Sorting the Player Details*/
	public void sorting() {
		Arrays.sort(players,0,j);
		displayElements();
		
}
	/*The Method will Search Date of Birth of the players*/
	public List<Player> searchDate(LocalDate date) {
		List<Player> playerList=new ArrayList<Player>();
			for(int i=0;i<j;i++) {
				if (players[i].getBirthDate().isAfter(date))
				{
				playerList.add(players[i]);
				}	
		}
		
		return playerList ;
	}
	public void delete(String name) {
		int k=0;
		for (int i = 0; i < players.length; i++) {
			if(!players[j].getPlayername().equals(name)) {
				players[k]=players[i];
				k++;
				System.out.println("successfully Deleted");
			}
		}
		j=k;
			
		}
	/* The Method will Write the data into a text file*/
	public void playerDownload() throws Exception{	
			FileWriter file=new FileWriter("playerDetails.csv");
			for(int i=0;i<j;i++)
			{
				String data=csvConversion(players[i]);
				file.write(data);
			}
				file.close();
	}
	public void playerDownloadJson() throws Exception{
		String data;
		StringJoiner join = new StringJoiner(",", "[", "]");



		FileWriter file = new FileWriter("download.json");
		for (int i = 0; i < j; i++) {
		data = asJson(players[i]);
		join.add(data);
		}
		file.write(join.toString());
		file.close();
	}
	public int getJ() {
		return j;
	}
	public void setJ(int j) {
		this.j = j;
	}
}

	

