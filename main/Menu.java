package main;
import java.io.FileReader;
import java.time.LocalDate;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;


public class Menu {

	public static void main(String[] args) throws Exception {
		Scanner scanner=new Scanner(System.in);
		int ch;
		FileReader reader=new FileReader("src/config.properties");
		Properties properties=new Properties();
		properties.load(reader);
		PlayerService playerService=PlayerServiceFactory.getPlayerservice("Array",properties);
		do{
			System.out.println("1.Add\n2.Display\n3.Search\n4.Sort\n5.SearchDate\n6.delete\n7.download\n8.Json\n9.Exit\nEnter the choice:");
			ch=scanner.nextInt();
			switch(ch) {
			case 1:
				try {
				System.out.println("enter the team name:");
				String t=scanner.next();
				System.out.println("enter the player name:");
				String p=scanner.next();
				System.out.println("enter the playernumber");
				int n=scanner.nextInt();
				System.out.println("Enter the date of birth:");
				String date=scanner.next();
				LocalDate d=LocalDate.parse(date);
				System.out.println("Enter the Gender");
				String gender=scanner.next();
				Gender g=Gender.valueOf(gender.toUpperCase());
				Player player=new Player(t,p,n,d,g);
				playerService.add(player);
				}catch(Exception exception) {
					System.out.println("Exception");
				}
				break;
			case 2:
				playerService.displayElements();
				break;
			case 3:
				System.out.println("search element:");
				String name=scanner.next();
				playerService.searchElement(name);
				break;
			case 4:
				playerService.sorting();
				break;
			case 5:
				System.out.println("Enter the date:");
				String checkDate=scanner.next();
				LocalDate d=LocalDate.parse(checkDate);
				List<Player> players=playerService.searchDate(d);
				if(players==null)
				{
					System.out.println("No data");
				}
				else
				System.out.println(players);
				break;
			case 6:
				System.out.println("delted item:");
				String name1=scanner.next();
				playerService.delete(name1);
			case 7:
				try {
					playerService.playerDownload();
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case 8:
				try {
					playerService.playerDownloadJson();
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case 9:
				System.exit(0);
				break;
			default:
				System.out.println("invalid");
				break;
			}
		}while(ch!=9);
	
		scanner.close();

	} 

}
