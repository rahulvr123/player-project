package main;

import java.util.Properties;

public abstract class PlayerServiceFactory {
	public static PlayerService getPlayerservice(String type,Properties properties)
	{
		PlayerService service=null;
		switch(type) {
		case "Array":
			service=new PlayerServiceArrayImpl();
			break;
		case "ArrayList":
			service=new PlayerServiceArrayListImpl();
			break;
		case "DataBase":
			String driver=properties.getProperty("driver");
			String url=properties.getProperty("url");
			String user=properties.getProperty("user");
			String password=properties.getProperty("password");
			service=new PlayerServiceDatabaseImpl(user, password,driver,url);
		}
		
		return service;
	}

}
