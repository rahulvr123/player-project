package main;
import java.time.LocalDate;
import java.util.List;
public interface PlayerService {
	
	void add(Player player);
	void displayElements();
	void searchElement(String name);
	void sorting();
	List<Player> searchDate(LocalDate date);
	void playerDownload() throws Exception;
	default String csvConversion(Player players) {
			return new StringBuffer().append("\"").append(players.getPlayername()).append("\",\"").append(players.getTeamname()).append("\",\"").append(players.getPlayernumber()).append("\",\"").append(players.getBirthDate()).append("\",\"").append(players.getGender()).append("\"\n").toString();
		}
	void playerDownloadJson() throws Exception;
	default String asJson(Player players) {
		return new StringBuffer().append("{").append("\"PlayerName\":").append("\"").append(players.getPlayername()).append("\"").append(",\"TeamName\":").append("\"").append(players.getTeamname()).append("\"").append(",\"PlayerNumber\":").append(players.getPlayernumber()).append(",\"BirthDate\":").append("\"").append(players.getBirthDate()).append("\"").append(",\"Gender\":").append("\"").append(players.getGender()).append("\"").append("}").append("\n").toString();
	}
	void delete(String name);
}
