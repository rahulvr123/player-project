package main;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class PlayerServiceArrayListImpl implements PlayerService {
	List<Player> playerList=new ArrayList<>();
	@Override
	public void add(Player player) {
		if(playerList.size()>0 && playerList.contains(player)) {
			System.out.println("Already exist");

			}
		else
		playerList.add(player);
	}

	@Override
	public void displayElements() {
		//playerList.forEach(data->System.out.println("****\n"+data+"****"));
		playerList.forEach(System.out::println);
	}

	@Override
	public void searchElement(String name) {
		System.out.println(playerList.stream().filter(player -> player.getPlayername().equalsIgnoreCase(name))
		.collect(Collectors.toList()));
	}

	@Override
	public void sorting() {
		Collections.sort(playerList);
		displayElements();
	}

	@Override
	public List<Player> searchDate(LocalDate date) {
		
		return playerList.stream()
			.filter(playerlist->playerlist.getBirthDate().isAfter(date))
			.collect(Collectors.toList());
	}
	public void delete(String name) {
//		for(int i=0;i<playerList.size();i++) {
//			if (playerList.get(i).getPlayername().equalsIgnoreCase(name)) {
//				playerList.remove(i);
//			}
//		}
		for (Iterator<Player> iter = playerList.iterator(); iter.hasNext();) {
			Player coffee = iter.next();
			if (coffee.getPlayername().equals(name)) {
			iter.remove();
			}
		}
	}

	@Override
	public void playerDownload() throws Exception {
		FileWriter file=new FileWriter("playerDetails.csv");
		for(int i=0;i<playerList.size();i++)
		{
			String data=csvConversion(playerList.get(i));
			file.write(data);
		}
			file.close();
	}

	@Override
	public void playerDownloadJson() throws IOException {
		String data;
		StringJoiner join = new StringJoiner(",", "[", "]");
		FileWriter file = new FileWriter("download.json");
		for (Player player : playerList) {
		data = asJson(player);
		join.add(data);
		}
		file.write(join.toString());
		file.close();
	}
	
}

