package main;


import java.time.LocalDate;
import java.util.Objects;

public class Player implements Comparable<Player>
{
	private String teamName,playerName;
	private int playerNumber=0;
	private LocalDate birthDate;
	private Gender gender;
	public Gender getGender() {
		return gender;
	}
	public Player(String teamName,String playerName,int playerNumber,LocalDate birthDate,Gender gender) {
		this.teamName=teamName;
		this.playerName=playerName;
		this.playerNumber=playerNumber;
		this.birthDate=birthDate;
		this.gender=gender;
	}
	public Player() {
		
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthdate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
	public String getTeamname() {
		return teamName;
	}
	public void setTeamname(String teamName) {
		this.teamName = teamName;
	}
	public String getPlayername() {
		return playerName;
	}
	public void setPlayername(String playerName) {
		this.playerName = playerName;
	}
	public int getPlayernumber() {
		return playerNumber;
	}
	public void setPlayernumber(int playerNumber) {
		this.playerNumber = playerNumber;
	}
	//toString method and StringBuffer/StringBuilder method
	public String toString()
	{	
		return new StringBuffer().append("Team Name:").append(teamName).append("\n").append("Player Name:").append(playerName).append("\n").append("PlayerNumber:").append(playerNumber).append("\n").append("Birth Date:").append(birthDate).append("\n").append(gender).toString();		
	}
	@Override
	public int compareTo(Player player) {
		return (playerNumber-player.playerNumber);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		return Objects.equals(birthDate, other.birthDate) && gender == other.gender
				&& Objects.equals(playerName, other.playerName) && playerNumber == other.playerNumber
				&& Objects.equals(teamName, other.teamName);
	}
	
	

}
